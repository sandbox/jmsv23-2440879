var ChatMessageForm = React.createClass({
  onSubmit: function(e){
    e.preventDefault();
    
    if(this.refs.message.getDOMNode().value != '') {
      this.props.sendMessage(this.refs.message.getDOMNode().value);
      this.refs.message.getDOMNode().value = '';  
    }
  },
  render: function() {
    return (
      <form action="" id="node-chat" onSubmit={this.onSubmit}>
          <input id="node-chat-input" className="node-chat-input" autocomplete="off" ref="message" /><button>Send</button>
      </form>
    )
  }
});
var ChatMessage = React.createClass({
  render: function() {
    return (
      <li>
        <span className="node-chat-user">{this.props.user}</span>
        <span dangerouslySetInnerHTML={{__html: this.props.msg}} />
      </li>
    )
  }
});

var ChatEmoji = React.createClass({
  handlerClick: function(e) {
    e.preventDefault();
    var emoji = this.refs.emoji.getDOMNode();
    this.props.sendEmoji(emoji.dataset.shortname);
  },
  render: function() {
    return (
      <span ref="emoji" onClick={this.handlerClick} data-shortname={this.props.shortname} dangerouslySetInnerHTML={{__html: this.props.renderEmoji}}/>
    )
  }
});

var ChatEmojisWrapper = React.createClass({
  render: function() {
    var self = this;
    var items = this.props.emojis.map(function(emoji, index){
      return (
        <ChatEmoji sendEmoji={self.props.sendEmoji} shortname={emoji} renderEmoji={emojione.toImage(emoji)}/>
      )
    });
    return (
      <div className="node-chat-emoji-wrapper">
        <a href="http://emoji.codes/" target="_blank">More +</a>
        <div className="node-chat-emojis">
          {items}
        </div>
      </div>
    )
  }
});

var ChatMessageList = React.createClass({
  render: function() {
    var items = this.props.messages.map(function(msg, index){
      return (
        <ChatMessage user={msg.user} msg={msg.msg}/>
      )
      });
    return (
      <ul id="node-chat-messages" className="node-chat-messages">
        {items}
      </ul>
    )
  }
});

var ChatWindow = React.createClass({
  socket: null,
  getInitialState: function(){
    return {messages: [], windowClasses: ["node-chat-window", "node-chat-window-no-display"]};
  },
  updateMessage: function(msg) {
    //parse links
    msg.msg = this.parseLinks(msg.msg);

    //add emoji support
    msg.msg = emojione.toImage(msg.msg);
    
    this.state.messages.push(msg);

    this.setState({messages: this.state.messages});
    var ml = this.refs.chatMessagesList.getDOMNode();
    ml.scrollTop = ml.scrollHeight;
  },
  parseLinks: function(msg) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = msg.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
  },
  componentDidMount: function(){
    var serviceURL = Drupal.settings.nodeChat.serverIp + (Drupal.settings.nodeChat.port != '')? ':' + Drupal.settings.nodeChat.port : '';
    this.socket = io(serviceURL);
    var self = this;
    this.socket.on('server message', function(msg){
      self.updateMessage(msg);
    });
  },
  sendMessage: function(msg) {
    var obj = {
      msg: msg,
      user: Drupal.settings.nodeChat.name
    };
    this.socket.emit('chat message', obj);
  },
  sendEmoji: function(emoji) {
    var obj = {
      msg: emoji,
      user: Drupal.settings.nodeChat.name
    };
    this.socket.emit('chat message', obj);
  },
  toggleWindow: function() {
    var indexClass = this.state.windowClasses.indexOf("node-chat-window-no-display");
    if(indexClass == -1) {
      this.state.windowClasses.push("node-chat-window-no-display");
    } else {
      delete this.state.windowClasses[indexClass];
    }
    this.setState({windowClasses: this.state.windowClasses});
  },
  render: function() {
    return (
      <div className={this.state.windowClasses.join(" ")}>
        <ChatEmojisWrapper emojis={emoticons} sendEmoji={this.sendEmoji} />
        <h2 onClick={this.toggleWindow}>{Drupal.settings.nodeChat.windowName}</h2>
        <ChatMessageList ref="chatMessagesList" messages={this.state.messages}/>
        <ChatMessageForm ref="chatMessageForm" sendMessage={this.sendMessage}/>
      </div>
    )
  }
});

var ChatBar = React.createClass({
  render: function() {
    return (
      <div id="chat-bar-bottom">
        <ChatWindow />
      </div>
    )
  }
});

(function($){
  $(document).ready(function(){
    var nodeChatBar = $(document.createElement('div'));
    nodeChatBar.attr('id', 'node-chat-bar').appendTo('body');
    React.render(
      <ChatBar />,
      document.getElementById("node-chat-bar")
    );    
  });
})(jQuery)
