var ChatMessageForm = React.createClass({displayName: "ChatMessageForm",
  onSubmit: function(e){
    e.preventDefault();
    
    if(this.refs.message.getDOMNode().value != '') {
      this.props.sendMessage(this.refs.message.getDOMNode().value);
      this.refs.message.getDOMNode().value = '';  
    }
  },
  render: function() {
    return (
      React.createElement("form", {action: "", id: "node-chat", onSubmit: this.onSubmit}, 
          React.createElement("input", {id: "node-chat-input", className: "node-chat-input", autocomplete: "off", ref: "message"}), React.createElement("button", null, "Send")
      )
    )
  }
});
var ChatMessage = React.createClass({displayName: "ChatMessage",
  render: function() {
    return (
      React.createElement("li", null, 
        React.createElement("span", {className: "node-chat-user"}, this.props.user), 
        React.createElement("span", {dangerouslySetInnerHTML: {__html: this.props.msg}})
      )
    )
  }
});

var ChatEmoji = React.createClass({displayName: "ChatEmoji",
  handlerClick: function(e) {
    e.preventDefault();
    var emoji = this.refs.emoji.getDOMNode();
    this.props.sendEmoji(emoji.dataset.shortname);
  },
  render: function() {
    return (
      React.createElement("span", {ref: "emoji", onClick: this.handlerClick, "data-shortname": this.props.shortname, dangerouslySetInnerHTML: {__html: this.props.renderEmoji}})
    )
  }
});

var ChatEmojisWrapper = React.createClass({displayName: "ChatEmojisWrapper",
  render: function() {
    var self = this;
    var items = this.props.emojis.map(function(emoji, index){
      return (
        React.createElement(ChatEmoji, {sendEmoji: self.props.sendEmoji, shortname: emoji, renderEmoji: emojione.toImage(emoji)})
      )
    });
    return (
      React.createElement("div", {className: "node-chat-emoji-wrapper"}, 
        React.createElement("a", {href: "http://emoji.codes/", target: "_blank"}, "More +"), 
        React.createElement("div", {className: "node-chat-emojis"}, 
          items
        )
      )
    )
  }
});

var ChatMessageList = React.createClass({displayName: "ChatMessageList",
  render: function() {
    var items = this.props.messages.map(function(msg, index){
      return (
        React.createElement(ChatMessage, {user: msg.user, msg: msg.msg})
      )
      });
    return (
      React.createElement("ul", {id: "node-chat-messages", className: "node-chat-messages"}, 
        items
      )
    )
  }
});

var ChatWindow = React.createClass({displayName: "ChatWindow",
  socket: null,
  getInitialState: function(){
    return {messages: [], windowClasses: ["node-chat-window", "node-chat-window-no-display"]};
  },
  updateMessage: function(msg) {
    //parse links
    msg.msg = this.parseLinks(msg.msg);

    //add emoji support
    msg.msg = emojione.toImage(msg.msg);
    
    this.state.messages.push(msg);

    this.setState({messages: this.state.messages});
    var ml = this.refs.chatMessagesList.getDOMNode();
    ml.scrollTop = ml.scrollHeight;
  },
  parseLinks: function(msg) {
    var replacedText, replacePattern1, replacePattern2, replacePattern3;

    //URLs starting with http://, https://, or ftp://
    replacePattern1 = /(\b(https?|ftp):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/gim;
    replacedText = msg.replace(replacePattern1, '<a href="$1" target="_blank">$1</a>');

    //URLs starting with "www." (without // before it, or it'd re-link the ones done above).
    replacePattern2 = /(^|[^\/])(www\.[\S]+(\b|$))/gim;
    replacedText = replacedText.replace(replacePattern2, '$1<a href="http://$2" target="_blank">$2</a>');

    //Change email addresses to mailto:: links.
    replacePattern3 = /(([a-zA-Z0-9\-\_\.])+@[a-zA-Z\_]+?(\.[a-zA-Z]{2,6})+)/gim;
    replacedText = replacedText.replace(replacePattern3, '<a href="mailto:$1">$1</a>');

    return replacedText;
  },
  componentDidMount: function(){
    var serviceURL = Drupal.settings.nodeChat.serverIp + (Drupal.settings.nodeChat.port != '')? ':' + Drupal.settings.nodeChat.port : '';
    this.socket = io(serviceURL);
    var self = this;
    this.socket.on('server message', function(msg){
      self.updateMessage(msg);
    });
  },
  sendMessage: function(msg) {
    var obj = {
      msg: msg,
      user: Drupal.settings.nodeChat.name
    };
    this.socket.emit('chat message', obj);
  },
  sendEmoji: function(emoji) {
    var obj = {
      msg: emoji,
      user: Drupal.settings.nodeChat.name
    };
    this.socket.emit('chat message', obj);
  },
  toggleWindow: function() {
    var indexClass = this.state.windowClasses.indexOf("node-chat-window-no-display");
    if(indexClass == -1) {
      this.state.windowClasses.push("node-chat-window-no-display");
    } else {
      delete this.state.windowClasses[indexClass];
    }
    this.setState({windowClasses: this.state.windowClasses});
  },
  render: function() {
    return (
      React.createElement("div", {className: this.state.windowClasses.join(" ")}, 
        React.createElement(ChatEmojisWrapper, {emojis: emoticons, sendEmoji: this.sendEmoji}), 
        React.createElement("h2", {onClick: this.toggleWindow}, Drupal.settings.nodeChat.windowName), 
        React.createElement(ChatMessageList, {ref: "chatMessagesList", messages: this.state.messages}), 
        React.createElement(ChatMessageForm, {ref: "chatMessageForm", sendMessage: this.sendMessage})
      )
    )
  }
});

var ChatBar = React.createClass({displayName: "ChatBar",
  render: function() {
    return (
      React.createElement("div", {id: "chat-bar-bottom"}, 
        React.createElement(ChatWindow, null)
      )
    )
  }
});

(function($){
  $(document).ready(function(){
    var nodeChatBar = $(document.createElement('div'));
    nodeChatBar.attr('id', 'node-chat-bar').appendTo('body');
    React.render(
      React.createElement(ChatBar, null),
      document.getElementById("node-chat-bar")
    );    
  });
})(jQuery)
